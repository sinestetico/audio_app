'use strict';

var socketio = require('socket.io');
var loopback = require('loopback');
var boot = require('loopback-boot');
var http = require('http');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  var server = app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  app.io = socketio.listen(server);

  app.io.on('connection', function(socket) {
    // manage web socket events
    socket.on('play', function(msg) {
      console.log('play', msg);
      app.models.Event.create({name: 'play', createdAt: Date.now(), data: msg});
      socket.broadcast.emit('play', msg);
    });
    socket.on('loop', function(msg) {
      console.log('loop', msg);
      app.models.Event.create({name: 'loop', createdAt: Date.now(), data: msg});
      socket.broadcast.emit('loop', msg);
    });
    socket.on('reset', function(msg) {
      console.log('reset', msg);
      app.models.Event.create({name: 'reset', createdAt: Date.now(), data: msg});
      socket.broadcast.emit('reset', {});
    });
    socket.on('config', function(msg) {
      console.log('config', msg);
      app.models.Event.create({name: 'config', createdAt: Date.now(), data: msg});
      //socket.broadcast.emit('config', msg);
      // send the config to the gui app
      var options = {
        hostname: 'localhost',
        port: 4001,
        path: '/api/Events/',
        method: 'POST',
        headers: {'Content-Type': 'application/json'}
      };
      var params = {name: 'config', data: msg};
      var req = http.request(options, function(res) {});
      req.write(JSON.stringify(params));
      req.end();
    });
  });

  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
  }
});
