// app managed by
var app = angular.module('hrApp', ['lbServices', 'ui.router']);

app.config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider) {

  $urlRouterProvider
    .when('/', '/main')
    .otherwise('/#');

  $stateProvider
    .state('main', {
      url: '/main',
      controller: 'mainCtrl',
      templateUrl: 'js/components/main/mainView.html',
    })
    .state('age', {
      url: '/age',
      controller: 'ageCtrl',
      templateUrl: 'js/components/age/ageView.html',
    })
    .state('config', {
      url: '/config/:age',
      controller: 'configCtrl',
      templateUrl: 'js/components/config/configView.html',
    })
    .state('player', {
      url: '/player',
      controller: 'playerCtrl',
      templateUrl: 'js/components/player/playerView.html',
    })
}]);

app.run(['$rootScope', function($rootScope) {
  $rootScope.socket = io();
}]);