app.controller('playerCtrl', ['$scope','$rootScope','$state','Resource',
                              function($scope,$rootScope,$state,Resource) {
  $scope.tracks = [];
  $scope.audio = null;
  $scope.loop = false;
  $scope.playing = null;

  $rootScope.socket.on('loop', function(msg) {
    console.log('loop', msg);
    $scope.loop = true;
    $scope.playing = 0;
    $scope.loadTrack(0);
    $scope.$apply();
  });
  $rootScope.socket.on('play', function(msg) {
    console.log('play', msg);
    $scope.loadTrack(msg.index);
    $scope.playing = msg.index;
    $scope.$apply();
  });
  $rootScope.socket.on('reset', function(msg) {
    console.log('reset', msg);
    $scope.audio.pause();
    $scope.playing = null;
    $scope.loop = false;
    $scope.$apply();
  });

  // gestisce l'uscita dall'applicazione
  $('#myModal').on('hidden.bs.modal', function () {
    $rootScope.socket.emit('reset');
    $scope.loadTrack(0, false);
    $state.go('main');
  });

  // retrieve all audio files
  Resource.getFiles({container: 'audio'})
    .$promise
    .then(function(files) { $scope.tracks = files; })
    .catch(function(err) { console.error(err); });
  // return the media file URL
  $scope.getURL = function(i) {
    var mediaPath = '/api/Resources/audio/download/';
    return mediaPath + $scope.tracks[i].name;
  }
  
  // carica la traccia per
  $scope.loadTrack = function(i, play=true) {
    $scope.audio.pause();
    $scope.audio.src = $scope.getURL(i);
    $scope.audio.play();

  };
  // gestisce la fine dell'esecuzione di una traccia
  $scope.onEnded = function() {
    console.log('ended track');
    if ($scope.loop) {
      $scope.playing += 1;
      $scope.playing %= $scope.tracks.length;
      $scope.loadTrack($scope.playing);
      $rootScope.socket.emit('play', {
        index: $scope.playing,
        name: $scope.tracks[$scope.playing].name
      });
      $scope.$apply();
    }
  };
  // avvia l'esecuzione in loop delle tracce audio
  $scope.startLoop = function() {
    console.log('loop')
    $scope.loop = true;
    $scope.playing = 0;
    $scope.loadTrack(0);
    $rootScope.socket.emit('loop', {});
  };
  // gestisce la riproduzione di una traccia singola
  $scope.playTrack = function(i) {
    $scope.playing = i;
    $scope.loop = false;
    $scope.loadTrack(i);
  }
  // gestisce l'interruzione di una traccia
  $scope.stopTrack = function() {
    $scope.audio.pause();
    $scope.playing = null;
    $scope.loop = false;
    $rootScope.socket.emit('reset', {});
  }

  // Add user agent as an attribute on the <html> tag
  var b =   document.documentElement;
  b.setAttribute('data-useragent', navigator.userAgent);
  b.setAttribute('data-platform', navigator.platform);
  // HTML5 audio player + playlist controls
  var supportsAudio = !!document.createElement('audio').canPlayType;
  if (!supportsAudio) {
    throw new Error('HTML5 player not supported');
  }
  $scope.audio = $('#audio1').bind('ended', $scope.onEnded).get(0);
}]);
