app.controller('configCtrl', ['$scope','$rootScope','$state','$stateParams',
                              function($scope,$rootScope,$state,$stateParams) {
	$scope.num = [];
  $scope.selection = {};
  $scope.age = $stateParams.age;
  // inizializza l'elenco delle scelte
  for(var i=0; i<30; i++) {
    $scope.num.push(i);
    $scope.selection[i] = null;
  }
  // imposta il sesso di un utente
  $scope.setUser = function(position, choice) {
    $scope.selection[position] = choice;
  };
  // invia i dati al server
  $scope.sendSelection = function() {
    console.log('Utenti selezionati', $scope.age, $scope.selection);
    $rootScope.socket.emit('config', {age: $scope.age, pos: $scope.selection});
    $state.go('player');
  }
}]);